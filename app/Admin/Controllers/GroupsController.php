<?php

namespace App\Admin\Controllers;

use App\Auditory;
use App\Groups;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class GroupsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
{
    $grid = new Grid(new Groups());

    $grid->column('id', __('ID'))->sortable();
    $grid->column('name', __('номер группы'));
    return $grid;
}

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
{
    $show = new Show(Groups::findOrFail($id));

    $show->field('id', __('ID'));
    $show->field('name', __('номер группы'));

    return $show;
}

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
{
    $form = new Form(new Groups);

    $form->text('id', __('ID'));
    $form->text('name', __('номер группы'));

    return $form;
}
}
