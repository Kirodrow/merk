<?php

namespace App\Admin\Controllers;

use App\Auditory;
use App\Groups;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Http\Request;


class ScheduleController extends Controller
{

    public function createSchedule()
    {
        $groups = Groups::all()->pluck('name', 'name');

        $data = ['groups' => $groups];
        return view('addParams')->with($data);

    }

    public function createScheduleSecondStep(Request $request)
    {
        $post = $request->all();
        $auditory = Auditory::all()->pluck('name', 'id');
        $lessons = Lesson::all()->pluck('name', 'id');
        $weeks = Helper::weeks();
        $typeLesson = Helper::typeLesson();
        $daysOfWeek = Helper::daysOfWeek();
        $data = ['post' => $post, 'auditory' => $auditory, 'weeks' => $weeks, 'typeLesson' => $typeLesson, 'daysOfWeek' => $daysOfWeek, 'lessons' => $lessons];

        return view('addParamsSecond')->with($data);

    }

    public function createScheduleThirdStep(Request $request)
    {
        dd($_POST);
    }
}
