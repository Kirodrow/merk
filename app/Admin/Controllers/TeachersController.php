<?php

namespace App\Admin\Controllers;

use App\Auditory;
use App\Teachers;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class TeachersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Example controller';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Teachers());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Имя преподавателя'));
        $grid->column('surname', __('Фамилия преподавателя'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Teachers::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Имя преподавателя'));
        $show->field('surname', __('Фамилия преподавателя'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Teachers);

        $form->text('id', __('ID'));
        $form->text('name', __('Имя преподавателя'));
        $form->text('surname', __('Фамилия преподавателя'));

        return $form;
    }
}
{
    //
}
