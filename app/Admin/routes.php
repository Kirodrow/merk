<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->resource('/auditories', 'AuditoriesController');
    $router->resource('/groups', 'GroupsController');
    $router->resource('/teachers', 'TeachersController');
    $router->resource('/lessons', 'LessonController');
    $router->get('/params', 'ScheduleController@createSchedule');
    $router->post('/secondStep', 'ScheduleController@createScheduleSecondStep');
    $router->post('/thirdStep', 'ScheduleController@createScheduleThirdStep');

});
