<?php


namespace App\Helpers;


class Helper
{

    public static function daysOfWeek()
    {
        return [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
            '7' => 'Воскресенье'];
    }

    public  static function weeks ()
    {
        return [
            '1' => 'Первая',
            '2' => 'Вторая',
            '4' => 'Третья',
            '5' => 'Четвертая',
        ];
    }

    public static function typeLesson ()
    {
        return [
            '1' => 'Лекция',
            '2' => 'Семинар',
            '4' => 'Практическая',
            '5' => 'Лабораторная',
        ];
    }

}
