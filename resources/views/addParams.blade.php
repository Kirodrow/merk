@extends('admin::index', ['header' =>'Рассписание'])

@section('content')
    <section class="content-header">

        <!-- breadcrumb start -->


        <!-- breadcrumb end -->

    </section>

    <section class="content">
        {{ Form::open(array('url' => 'admin/secondStep')) }}
        <div class="row">

            <div class="col-md-4">
                <h1>Группа</h1>
                {{Form::select('group[]', $groups, [], ['multiple'=>'multiple','class'=> 'form-control'])}}
            </div>
            <div class="col-md-4">
                <h1>Выбор недели</h1>
                {{Form::select('weeks[]', \App\Helpers\Helper::weeks(), [], ['multiple'=>'multiple','class'=> 'form-control'])}}
            </div>
            <div class="col-md-4">
                <h1>День недели</h1>
                {{Form::select('dayweeks[]', \App\Helpers\Helper::daysOfWeek(), [], ['multiple'=>'multiple','class'=> 'form-control'])}}
            </div>

        </div>
        <div class="row margin-r-5">
            <div class="col-md-2">
                {{ Form::submit('Отправить', ['class' =>'form-control' ]) }}
            </div>
        </div>
        {{ Form::close() }}

    </section>
@endsection
