@extends('admin::index', ['header' =>'Рассписание'])

@section('content')
    <section class="content-header">

        <!-- breadcrumb start -->


        <!-- breadcrumb end -->

    </section>

    <section class="content">
        {{ Form::open(array('url' => 'admin/thirdStep')) }}

        @foreach($post['group'] as $group)
            @foreach($post['weeks'] as $weeks)
                @foreach($post['dayweeks'] as $key =>  $dayweeks)
                    <div style="border: 1px solid sandybrown">
                        <div class="row">

                            <div class="col-md-2">
                                Пара №
                            </div>
                            <div class="col-md-2">
                                Предмет
                            </div>
                            <div class="col-md-2">
                                Вид пары
                            </div>
                            <div class="col-md-2">
                                Аудитория
                            </div>


                        </div>
                        @for($i=1; $i <= 4; $i++)

                            @php
                              $keyForPost = $group.'['.$weeks.']['.$dayweeks.']['.$i.']';
                            @endphp


                            <div class="row">

                                <div class="col-md-2">
                                   {{$i}}
                                </div>
                                <div class="col-md-2">
                                    {{Form::select($keyForPost.'[lesson]', $lessons, [], ['class'=> 'form-control'] )}}
                                </div>
                                <div class="col-md-2">
                                    {{Form::select($keyForPost.'[typeLesson]' , \App\Helpers\Helper::typeLesson(), [], ['class'=> 'form-control'] )}}
                                </div>
                                <div class="col-md-2">
                                    {{Form::select($keyForPost.'[typeLesson]' , $auditory, [], ['class'=> 'form-control'] )}}
                                </div>


                            </div>
                        @endfor
                    </div>
                @endforeach
            @endforeach
        @endforeach
        <div class="row">
            <div class="col-md-4">
                {{Form::submit()}}
            </div>
        </div>
        {{ Form::close() }}

    </section>
@endsection
